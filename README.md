Statut du 06.2022 les conditions qui explicitent la modification de ces statuts est décrite dans l'ART.4

### ART.1 - NOM
 
Il est fondé une association régie par la loi du 1er juillet 1901 et le décret du 16 août 1901, ayant pour titre : Association pour la Rotation et la Sortition (l'APRES). Cet acronyme ainsi que le nom de l'associationtion sont déposés sous la licence MIT depuis 2021.
 
### ART.2 - BUT

L'APRES fournit des outils pour faciliter les discussions et pour bâtir des collectifs plus solides. Notre association a l'originalité d'appliquer ses outils en premier lieu à elle-même dans le but de les parfaire. Les discussions au sein de l'association sont modérées à tour de rôle et la rotation se fait de manière systématique après un temps fixe. L'APRES fournit aussi un ensemble d'outils pour introduire cette nouvelle manière d'intéragir et en facilite l'introduction.

### ART.3 - ADMISSION & RADIATION
 
L'association se compose d’un ensemble d’adhérent·e·s et d'un bureau fait de 3 d'entre eux/elles. Elle est ouverte à toutes et tous, sans condition ni distinction.
Une seule condition est nécessaire pour faire parti de l'association:
Au minimum deux adhérent·e·s (dont un·e membre du bureau) cooptent l’adhésion.

La qualité d'adhérent·e se perd par :
- Démission
- Décès
- Radiation prononcée par l'ensemble des membres du bureau. 
Les deux personnes co-fondatrices de l'association peuvent poser un véto sur cette radiation même si elles n'appartiennent plus au bureau.
 
### ART.4 - CONSTITUTION DU BUREAU

- Le bureau est initialement constitué des deux personnes co-fondatrices
- Lorsque le nombre de memnbres de l'association aura atteint 40 adhérent·e·s on designera par tirage au sort un·e troisème membre du bureau parmi les adhérent·e·s.
- à partir de ce moment le mandat d'un·e membre du bureau sera d'un an non-renouvelable.
- Ce mandat peut être interrompu à tout moment si l'adhérent·e le désire.
- Lorsque le palier des 40 adhérent·e·s est atteint on utilisera la sortition pour que le bureau soit toujours constitué de trois personnes.
- Pour faire partie du bureau on doit être adhérent·e de l'association depuis au moins un mois.

Si la ou les personnes désigné·e·s n'entrent pas en contact avec le bureau dans les 15 jours suivant le tirage on relancera alors une nouvelle sortition sans leur nom. Avant la sortition tout membre peut demander à être retirer de la liste des personnes pouvant être selectionnée. Pour avoir plus de détail sur comment s'organise la sortition merci de consulter nos coutumes.

### ART.5 - RÔLE DU BUREAU
 
Toutes les personnes du bureau sont co-présidentes de l'association. Elles ont des droits d'administration sur le serveur Discord de l'association ainsi que sa page web (sortition.fr). On modifie le ou la propritéaire du serveur Discord par un vote à l'unanimité.
 
N'importe qu'elle adhérent·e peut proposer une modification des status ou des coutumes (aka réglement intérieur) et le bureau se réunit un fois par mois ipour mettre en place ou non cas modifications.
 
### ART.6- COUTUMES
 
Un ensemble de [coutumes](https://framagit.org/apres/coutumes) (aussi appelé réglement intérieur de l'association) sont, tout comme les statuts, destinées à évoluer. Ces coutumes régissent le fonctionnement technique de l'APRES.
